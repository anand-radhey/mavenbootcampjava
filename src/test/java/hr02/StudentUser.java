package hr02;

import hr01.Student;

public class StudentUser {

	public static void main(String[] args) {
		Student st1 = new Student(101,"Anand");
		
		Student st2 = new Student(102,"Saurabh",91,93, 97);
		System.out.println("st1.id : " + st1.getId());
		System.out.println("st2.id : " + st2.getId());
		
		st1.setPhy(81);
		st1.setChem(75);
		st1.setMaths(95);
		
		System.out.println(st1);
		System.out.println(st2);
		//Object
		
		

	}

}
