package hr07;

public interface IDrawable {
     void draw();
     void erase();
}
