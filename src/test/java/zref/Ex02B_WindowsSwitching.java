package zref;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class Ex02B_WindowsSwitching {

    public static void main(String[] args) throws InterruptedException {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        //System.setProperty("webdriver.gecko.driver", "test\\resources\\drivers\\geckodriver.exe");
        //WebDriver driver = new FirefoxDriver();

        //driver.manage().window().maximize();


        driver.get("https://www.ataevents.org/");
        //System.out.println("Page  Title : "+ driver.getTitle());
        
      //*[@id="india"]/div[2]/div/div[1]/div/div/div[1]/figure/a
        driver.findElement(By.xpath("//*[@id=\"india\"]/div[2]/div/div[1]/div/div/div[1]/figure/a")).click();
        
        
        driver.manage().window().maximize();
        
      //*[@id="india"]/div[2]/div/div[2]/div/div/div[1]/figure/a
        driver.findElement(By.xpath("//*[@id=\"india\"]/div[2]/div/div[2]/div/div/div[1]/figure/a")).click();

        Set<String> setHandles = driver.getWindowHandles();
        
        
        for(         String ele    :setHandles) {
        	
        	System.out.println(ele);
        	driver.switchTo().window(ele);
        	
        	  Thread.sleep(5000);
        	
        	System.out.println("Window's Title : " + driver.getTitle());
        	
        	
        	
        }
        
       


}
}
