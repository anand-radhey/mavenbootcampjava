package hr05;

import java.util.Comparator;

class CricketPlayerComparator implements Comparator<CricketPlayer> {

    
    public int compare(CricketPlayer obj1, CricketPlayer obj2) {
        return (obj2.team.compareTo(obj1.team));
    }
}

public class CricketScoreComparator implements Comparator<CricketPlayer>{

    
    public int compare(CricketPlayer obj1, CricketPlayer obj2) {
        return (obj2.score - obj1.score);
    }
}