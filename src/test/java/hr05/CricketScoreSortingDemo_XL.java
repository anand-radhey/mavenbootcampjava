package hr05;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class CricketScoreSortingDemo_XL {
    public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException {

        String[][] datafromxl = utils.XLDataReaders.getExcelData("src//test//resources//data//Cricket_Score_India_Eng_SA.xlsx", "India");
        ArrayList<CricketPlayer> Criclist1 = new ArrayList<CricketPlayer>();
        for (int i = 0; i < datafromxl.length; i++){

            String country = datafromxl[i][0];
            String name = datafromxl[i][1];
            String strscore = datafromxl[i][2];
            int score = Integer.parseInt(strscore);

            CricketPlayer ct = new CricketPlayer(country, name, score);
            Criclist1.add(ct);
        }
        System.out.println("Indian Team before sorting\n" +Criclist1);
        Collections.sort(Criclist1);
        //Collections.sort(Criclist1,new CricketScoreComparator());
        System.out.println("Indian Team After sorting\n" +Criclist1);



    }
}