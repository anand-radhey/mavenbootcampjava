package hr08;

import java.util.Comparator;

public class StudentIdAscendingComparator implements Comparator<Student> {
    public int compare(Student obj1, Student obj2) {
        return (obj1.id - obj2.id);
        //return 0;
    }
}

class StIdDescComparator implements Comparator<Student> {
    public int compare(Student obj1, Student obj2) {
        return (obj2.id - obj1.id);
        //return 0;
    }
}

    class StudentAgeAscendingComparator implements Comparator<Student> {
        public int compare(Student obj1, Student obj2) {
            return (obj1.age - obj2.age);
            //return 0;
        }
    }

    class StAgeDescComparator implements Comparator<Student> {
        public int compare(Student obj1, Student obj2) {
            return (obj2.age - obj1.age);
            //return 0;
        }
    }

        class StudentNameAscendingComparator implements Comparator<Student> {
            public int compare(Student obj1, Student obj2) {
                return (obj1.name.compareTo(obj2.name));
                //return 0;
            }
        }

        class StNameDescComparator implements Comparator<Student> {
            public int compare(Student obj1, Student obj2) {
                return (obj2.name.compareTo(obj1.name));
                //return 0;
            }
        }






