package hr08;



import java.util.ArrayList;
import java.util.Collections;

public class StudentDemo {
    public static void main(String[] args) {
        Student st1 = new Student(123, "Anand Vyas", 25);
        Student st2 = new Student(201, "Virat ", 27);
        Student st3 = new Student(205, "Akshay Kumar", 20);
        Student st4 = new Student(301, "Rohit", 25);
        Student st5 = new Student(305, "Vinay", 24);
        Student st6 = new Student(401, "Nitin", 22);
        Student st7 = new Student(501, "Abhinav", 20);
        Student st8 = new Student(20, "Virat ", 30);
        ArrayList<Student> starlist = new ArrayList<Student>();
       // System.out.println("1. No. of elements in ArrayList "+ starlist.size());
        starlist.add(st1);
        starlist.add(st2);
        starlist.add(st3);
        starlist.add(st4);
        starlist.add(st5);
        starlist.add(st6);
        starlist.add(st7);
        starlist.add(st8);

        //starlist.add(5,st2);
        System.out.println("Before Sorting \n" + starlist);

        Collections.sort(starlist,new StudentIdAscendingComparator());

        System.out.println("After Sorting  by Id in Asc order \n" + starlist);

        Collections.sort(starlist, new StIdDescComparator());
        System.out.println("After Sorting  by Id in Desc order \n" + starlist);

        Collections.sort(starlist, new StNameDescComparator());
        System.out.println("After Sorting  by Name in Desc order \n" + starlist);

Collections.sort(starlist, new StNameDescComparator().thenComparing(new StudentAgeAscendingComparator()));
        System.out.println("After Sorting  by Name in Desc order THEN By Age in ASC \n" + starlist);

        //Collections.sort(starlist, new SortStudentNameComparator().thenComparing(new SortStudentAgeComparator())

    }
}
