package day1;

public class Student {
	// fields // attributes
	// methods
	
	private String name;
	private int roll;
	
	//Constructor Overloading
	public Student() {
		
		
	}
	
	public String getName() {
		return name;
	}

	public Student(String name, int roll) {
		
		this.name = name;
		this.roll = roll;
	}

	

	public int getRoll() {
		return roll;
	}

	public void setRoll(int roll) {
		this.roll = roll;
	}
	
	
	
	
	
	
	

}
